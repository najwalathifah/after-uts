from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls', namespace='main')),
    path('story7/', include('story7.urls', namespace='story7')),
    path('story8/', include('story8.urls', namespace='story8')),
    path('story9/', include('story9.urls', namespace='story9')),
]
