from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout, get_user_model
from .models import Story9
from .forms import LoginForm, SignUpForm

User = get_user_model()

def story9login(request):
    form = LoginForm(request.POST or None)
    context = {
        'form': form
    }
    print(request.user.is_authenticated)
    if form.is_valid():
        print(form.cleaned_data)
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            print("ERROR")
    return render(request, 'story9login.html', context=context)

def story9signup(request):
    form = SignUpForm(request.POST or None)
    context = {
        'form': form,
    }
    if form.is_valid():
        username = form.cleaned_data.get("username")
        email = form.cleaned_data.get("email")
        password = form.cleaned_data.get("password_first")
        User.objects.create_user(username, email, password)
        return HttpResponseRedirect('/story9')   
    return render(request, 'story9signup.html', context=context)

def story9logout(request):
    logout(request)
    return render(request, 'main.html')
