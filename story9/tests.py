from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from django.contrib.auth.models import User
from .views import story9login, story9logout, story9signup
from .models import Story9
from .forms import LoginForm, SignUpForm
from .apps import Story9Config

class URLsTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/story9/')
        self.assertEquals(response.status_code, 200)

class HTMLTest(TestCase):
    def test_login_html_template_exists(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9login.html')

    def test_signup_html_template_exists(self):
        response = Client().get('/story9/signup/')
        self.assertTemplateUsed(response, 'story9signup.html')

    def test_logout_html_template_exists(self):
        response = Client().get('/story9/logout/')
        self.assertTemplateUsed(response, 'main.html')

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.login_page = reverse('story9:login')
        self.signup_page = reverse('story9:signup')
        self.logout_page = reverse('story9:logout')
        self.user_new = User.objects.create_user('metawin', 'metawin@gmail.com', password='winmetawin')
        self.user_new.save()
        self.profile = Story9.objects.create(user=self.user_new)

    def test_GET_login(self):
        response = self.client.get(self.login_page, {
            'username': 'metawin', 
            'password':'winmetawin'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9login.html')

    def test_GET_signup(self):
        response = self.client.get(self.signup_page, {
            'username': 'metawin', 
            'email': 'metawin@gmail.com', 
            'password_first':'winmetawin', 
            'password_again':'winmetawin'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9signup.html')
    
    def test_GET_logout(self):
        response = self.client.get(self.logout_page)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main.html')

    def test_register_login_post(self):
        response = self.client.post(self.signup_page, data = {
            'username': 'metawin', 
            'email': 'metawin@gmail.com', 
            'password_first':'winmetawin', 
            'password_again':'winmetawin'
        })

        response = self.client.post(self.signup_page, data = {
            'username': 'metawin', 
            'email': 'metawin@gmail.com', 
            'password_first':'winmetawin', 
            'password_again':'winmeta'
        })

        response = self.client.post(self.signup_page, data = {
            'username': 'metawinopas', 
            'email': 'metawin@gmail.com', 
            'password_first':'winmetawin', 
            'password_again':'winmetawin'
        })

        response = self.client.post(self.login_page,data ={
            'username' : 'metawin',
            'password' : 'winmetawin'
        })
        self.assertEquals(response.status_code,302)

    def test_not_register_yet(self):
        response = self.client.post(self.login_page,data ={
            'username' : 'meta',
            'password' : 'metawinopas'
        })
        self.assertEquals(response.status_code,200)

class FormTest(TestCase):
    def test_form_is_valid(self):
        form_login = LoginForm(data={
            'username': 'metawin',
            'password': 'winmetawin',
        })

        form_regist = SignUpForm(data={
            'username': 'metawin', 
            'email': 'metawin@gmail.com', 
            'password_first':'winmetawin', 
            'password_again':'winmetawin'
        })
        self.assertTrue(form_regist.is_valid())

    def test_form_invalid(self):
        form_login = LoginForm(data={})
        self.assertFalse(form_login.is_valid())
        form_register = SignUpForm(data={})
        self.assertFalse(form_register.is_valid())

    def test_form_regist_is_exist(self):
        form_regist = SignUpForm(data={
            'username': 'metawin', 
            'email': 'metawin@gmail.com', 
            'password_first':'winmetawin', 
            'password_again':'winmetawin',
        })
        form_regist = SignUpForm(data={
            'username': 'metawin', 
            'email': 'metawin@gmail.com', 
            'password_first':'winmetawin', 
            'password_again':'winmetawin',
        })
        self.assertTrue(form_regist.is_valid())

class ModelTest(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user('metawin', password='winmetawin')
        self.new_user.save()
        self.profile = Story9.objects.create(
            user = self.new_user
        )
        self.response = self.client.login(
            username = 'metawin',
            password = 'winmetawin'
        )

    def test_instance_created(self):
        self.assertEqual(Story9.objects.count(), 1)

    def test_instance_is_correct(self):
        self.assertEqual(Story9.objects.first().user, self.new_user)

    def test_to_string(self):
        self.assertIn('metawin', str(self.new_user.username))


class AppTest(TestCase):
    def test_app(self):
        self.assertEquals(Story9Config.name, 'story9')
        self.assertEquals(apps.get_app_config('story9').name, 'story9')