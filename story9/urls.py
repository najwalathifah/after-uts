from django.urls import path, include, re_path
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.story9login, name='login'),
    path('signup/', views.story9signup, name='signup'),
    path('logout/', views.story9logout, name='logout'),
]