$( function() {
    $( "#accordionActivities" ).accordion({
        active: false,
        collapsible: true,
        
    })

    $( "#accordionExperiences" ).accordion({
        active: false,
        collapsible: true,
        
    });

    $( "#accordionAchievement" ).accordion({
        active: false,
        collapsible: true,
    });

    $( "#accordionHappyPills" ).accordion({
        active: false,
        collapsible: true,
    });

    $('.up').click(function() {
        var selectedAccordion = $(this).parent().parent().parent();
        selectedAccordion.insertBefore(selectedAccordion.prev());
        event.stopPropagation(); 
        event.preventDefault(); 
    })

    $('.down').click(function() {
        var selectedAccordion = $(this).parent().parent().parent();
        selectedAccordion.insertAfter(selectedAccordion.next());
        event.stopPropagation(); 
        event.preventDefault(); 
    })
});