$(document).ready(() => {
    $('#keyword').val('');
    $.ajax({
        method: 'POST',
        url: 'data?q=ika+natassa',
        success: function(data) {
            let books_table = $('tbody');
            var all_items = data.items;

            for (i=0; i<all_items.length; i++) {
                var number = i + 1;
                var image = all_items[i].volumeInfo.imageLinks.smallThumbnail;
                var title = all_items[i].volumeInfo.title;
                var author = all_items[i].volumeInfo.authors;

                var num = $('<td>').append(number);
                
                if (image == false) {
                    var img = $('<td>').append("-");
                } else {
                    var img = $('<td>').append("<img src=" + image + ">");
                }

                var ttl = $('<td>').append(title);

                if (author == false) {
                    var aut = $('<td>').append("-");
                } else {
                    var aut = $('<td>').append(author);
                }

                var content_row = $('<tr>').append(num, img, ttl, aut);

                $(books_table).append(content_row);
            }
        }
    });

    $("#keyword").keyup(function() {
        var input_keyword = $("#keyword").val();
        let books_table = $('tbody');
        if (input_keyword.length) {
            $.ajax({
            url: 'data?q=' + input_keyword,
            success: function(data) {
                var all_items = data.items;
                $(books_table).empty();
                
                for (i=0; i<all_items.length; i++) {
                    var number = i + 1;
                    var image = all_items[i].volumeInfo.imageLinks.smallThumbnail;
                    var title = all_items[i].volumeInfo.title;
                    var author = all_items[i].volumeInfo.authors;

                    var num = $('<td>').append(number);
                    
                    if (image == false) {
                        var img = $('<td>').append("-");
                    } else {
                        var img = $('<td>').append("<img src=" + image + ">");
                    }

                    var ttl = $('<td>').append(title);

                    if (author == false) {
                        var aut = $('<td>').append("-");
                    } else {
                        var aut = $('<td>').append(author);
                    }

                    var content_row = $('<tr>').append(num, img, ttl, aut);

                    $(books_table).append(content_row);
                }
            }
        });
        }; 
        $(books_table).empty();
    });
});