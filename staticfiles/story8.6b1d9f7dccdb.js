$(document).ready(() => {
    $("#keyword").keyup(function() {
        var input_keyword = $("#keyword").val();

        let books_table = $('tbody');
        if (input_keyword.length) {
            $.ajax({
            url: 'data?q=' + input_keyword,
            success: function(data) {
                var all_items = data.items;
                $(books_table).empty();
                
                for (i=0; i<all_items.length; i++) {
                    var image = all_items[i].volumeInfo.imageLinks.smallThumbnail;
                    var title = all_items[i].volumeInfo.title;
                    var author = all_items[i].volumeInfo.authors;
                    
                    if (image == false) {
                        var img = $('<td>').append("-");
                    } else {
                        var img = $('<td>').append("<img src=" + image + ">");
                    }

                    var ttl = $('<td>').append(title);

                    if (author == false) {
                        var aut = $('<td>').append("-");
                    } else {
                        var aut = $('<td>').append(author);
                    }

                    var content_row = $('<tr>').append(img, ttl, aut);

                    $(books_table).append(content_row);
                }
            }
        });
        } 
        $(books_table).empty();
    });
})