from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json

def story8(request):
    response = {}
    return render(request, 'story8.html', response)

def books_func(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    result = requests.get(url)
    data = json.loads(result.content)
    return JsonResponse(data, safe=False)