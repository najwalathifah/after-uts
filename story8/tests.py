from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from .views import story8, books_func
from .apps import Story8Config
import json

class URLsTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 200)

class HTMLTest(TestCase):
    def test_html_template_exists(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8.html')

class ViewsTest(TestCase):
    def test_view_func(self):
        found = resolve('/story8/')
        self.assertEquals(found.func, story8)

    def test_get_story8_func(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'story8.html')

    def test_books_func(self):
        response = Client().get('/story8/data?q=ika+natassa')
        self.assertEquals(response.status_code, 200)

class AppTest(TestCase):
    def test_app(self):
        self.assertEquals(Story8Config.name, 'story8')
        self.assertEquals(apps.get_app_config('story8').name, 'story8')