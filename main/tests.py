from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from .views import main
from .apps import MainConfig

class URLsTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

class HTMLTest(TestCase):
    def test_html_template_exists(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main.html')

class ViewsTest(TestCase):
    def test_view_func(self):
        found = resolve('/')
        self.assertEquals(found.func, main)

    def test_get_story7_func(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'main.html')

class AppTest(TestCase):
    def test_app(self):
        self.assertEquals(MainConfig.name, 'main')
        self.assertEquals(apps.get_app_config('main').name, 'main')