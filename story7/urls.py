from django.urls import path, include, re_path
from . import views

app_name = 'story7'

urlpatterns = [
    path('', views.story7, name='story7'),
]