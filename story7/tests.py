from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from .views import story7
from .apps import Story7Config

class URLsTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/story7/')
        self.assertEquals(response.status_code, 200)

class HTMLTest(TestCase):
    def test_html_template_exists(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7.html')

class ViewsTest(TestCase):
    def test_view_func(self):
        found = resolve('/story7/')
        self.assertEquals(found.func, story7)

    def test_get_story7_func(self):
        response = Client().get('/story7/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'story7.html')

class AppTest(TestCase):
    def test_app(self):
        self.assertEquals(Story7Config.name, 'story7')
        self.assertEquals(apps.get_app_config('story7').name, 'story7')